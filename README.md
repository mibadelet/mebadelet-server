# MeBadelet-Server

# How to perform a call request:

## Curl example:

```
curl --location --request POST \
'https://calling.api.sinch.com/calling/v1/callouts' \
--header 'Content-Type: application/json' \
--user 61f707dc-83ce-464d-87f3-ae869f453b87:YTQO82K4T0GcEFVp8brkjA== \
--data-raw '{
  "method": "ttsCallout",
  "ttsCallout": {
    "cli": "+447520651963",
    "domain": "pstn",
    "destination": {
      "type": "number",
      "endpoint": "+972507742288"
    },
    "locale": "en-US",
    "prompts": "#tts[Someone is at the door. Someone is at the door. Someone is at the door.]",
  }
}'
```

## Python example:

```
import requests

url = "https://calling.api.sinch.com/calling/v1/callouts"
payload="{\n  \"method\": \"ttsCallout\",\n  \"ttsCallout\": {\n    \"cli\": \"+447520651963\",\n    \"domain\": \"pstn\",\n    \"destination\": {\n      \"type\": \"number\",\n      \"endpoint\": \"+972507742288\"\n    },\n    \"locale\": \"en-US\",\n    \"prompts\": \"#tts[Someone is at the door. Someone is at the door. Someone is at the door.]\"\n  }\n}"
headers = {
  'Content-Type': 'application/json',
  'Authorization': 'Basic NjFmNzA3ZGMtODNjZS00NjRkLTg3ZjMtYWU4NjlmNDUzYjg3OllUUU84Mks0VDBHY0VGVnA4YnJrakE9PQ=='
}
response = requests.request("POST", url, headers=headers, data=payload)

print(response.json())
```

## JavaScript example:

```
var requestOptions = {
  method: 'POST',
  headers: { "Content-Type": "application/json", "Authorization": "Basic NjFmNzA3ZGMtODNjZS00NjRkLTg3ZjMtYWU4NjlmNDUzYjg3OllUUU84Mks0VDBHY0VGVnA4YnJrakE9PQ==" },
  body: "{ \
  \"method\": \"ttsCallout\", \
  \"ttsCallout\": { \
    \"cli\": \"+447520651963\", \
    \"domain\": \"pstn\", \
    \"destination\": { \"type\": \"number\", \"endpoint\": \"+972507742288\" }, \
    \"locale\": \"en-US\", \
    \"prompts\": \"#tts[Someone is at the door. Someone is at the door. Someone is at the door.]\" \
  }}"
};
fetch("https://calling.api.sinch.com/calling/v1/callouts", requestOptions)
  .then(response => response.json())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```
