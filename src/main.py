import socket
import threading
from time import sleep

def handle_client(client_socket: socket.socket, client_address):
    print(f'Accepted connection from {client_address[0]}:{client_address[1]}')
    
    while True:
        # Receive data from the client
        # data = client_socket.recv(1024).decode()
        if not client_socket.fileno():
            # If no data is received, the client has disconnected
            break

        response_headers = "HTTP/1.1 200 OK\r\n"
        response_headers += "Content-Type: text/html\r\n"
        response_headers += "\r\n"

        response_content = "<html><body><h1>Hello, World!</h1></body></html>"

        response = response_headers + response_content

        # Send the response back to the client
        client_socket.sendall(response.encode())
        sleep(3)
        # print(f'Received data from {client_address[0]}:{client_address[1]}: {data}')
        # Process the received data (e.g., send a response)
        # ...
        
    # Close the client socket
    client_socket.close()
    print(f'Connection closed with {client_address[0]}:{client_address[1]}')

def start_server(host, port):
    # Create a socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    # Bind the socket to a specific address and port
    server_socket.bind((host, port))
    
    # Listen for incoming connections
    server_socket.listen()
    print(f'Server listening on {host}:{port}')
    
    while True:
        # Accept a client connection
        client_socket, client_address = server_socket.accept()
        
        # Start a new thread to handle the client
        thread = threading.Thread(target=handle_client, args=(client_socket, client_address))
        thread.start()

# Start the server
start_server('localhost', 8080)